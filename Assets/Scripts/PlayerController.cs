﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

	private int secondsUntilNextLevel = 1;

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		grounded = true;

		if (objectPlayerCollidedWith.tag == "Exit") {
			Invoke("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}

		else if (objectPlayerCollidedWith.tag == "Exit2") {
			Invoke("LoadNewLevel2", secondsUntilNextLevel);
			enabled = false;
		}

		else if (objectPlayerCollidedWith.tag == "Enemy"){
			Invoke("LoadNewLevel3", secondsUntilNextLevel);
			enabled = false; 
			}
		}


	    private void LoadNewLevel()
	{
		Application.LoadLevel(0);
	}

	private void LoadNewLevel2()
	{
		Application.LoadLevel(2);
	}

	private void LoadNewLevel3()
	{
		Application.LoadLevel(3);
	}


	public float speed;
	public float jump;
	float moveVelocity;
	
	//Grounded Vars
	bool grounded = true;
	
	void Update ()
	{
		//Jumping
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.Z) || Input.GetKeyDown (KeyCode.W)) {
			if (grounded) {
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, jump);
			}
		}
		
		moveVelocity = 0;
		
		//Left Right Movement
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
			moveVelocity = -speed;
		}
		if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
			moveVelocity = speed;
		}
		
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveVelocity, GetComponent<Rigidbody2D> ().velocity.y);
		
	}

	void OnTriggerExit2D()
	{
		grounded = false;
	}
}

